package cat.itb.electronicsandgaming

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cat.itb.electronicsandgaming.Data.Marker
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.*
import com.google.firebase.ktx.Firebase

class SingletonMarkers:ViewModel() {
    private var markersList: ArrayList<Marker> = arrayListOf<Marker>()
    private var savedMarker: Marker?
    private var db = FirebaseFirestore.getInstance()
    var markersLiveData = MutableLiveData<List<Marker>>()
    init {
        savedMarker = null
        markersList.add(Marker(id = null, lat = (41.4534).toFloat(),lon = (2.1841).toFloat(),color= BitmapDescriptorFactory.HUE_RED,name= "ITB", image = null))
    }
    fun addToList(m:Marker){
        markersList.add(m)
    }
    fun listOfMarkers () = markersList
    fun saveMarker (m:Marker):Boolean{
        if(savedMarker == null){
            savedMarker = m
            return true
        }
        return false
    }
    fun resetSave(){
        savedMarker = null
    }
    fun getSavedMarker():Marker? = savedMarker
    fun saveImageSavedMarker(uriImage: String){
        savedMarker!!.image = uriImage
    }
    fun saveImageData(title:String, lat:Float, lon:Float, color:Float){
        savedMarker!!.lat = lat
        savedMarker!!.lon = lon
        savedMarker!!.name = title
        savedMarker!!.color = color
    }
   fun textFromValue(colorVal:Float):String{
       when (colorVal){
           BitmapDescriptorFactory.HUE_RED -> return "Red"
           BitmapDescriptorFactory.HUE_YELLOW -> return "Yellow"
           BitmapDescriptorFactory.HUE_ORANGE -> return "Orange"
           BitmapDescriptorFactory.HUE_MAGENTA -> return "Purple"
           else -> return "Red"
       }
   }
}