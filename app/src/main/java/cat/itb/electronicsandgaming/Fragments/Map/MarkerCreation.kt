package cat.itb.electronicsandgaming.Fragments.Map

import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.electronicsandgaming.Data.Marker
import cat.itb.electronicsandgaming.R
import cat.itb.electronicsandgaming.SingletonMarkers
import cat.itb.electronicsandgaming.databinding.FragmentMarkerCreationBinding
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage


class MarkerCreation : Fragment() {
    private lateinit var binding: FragmentMarkerCreationBinding
    private val viewmodel: SingletonMarkers by activityViewModels()
    private val db = FirebaseFirestore.getInstance()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMarkerCreationBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var markerColor = 0f
        var latitude:Float = 0f
        var longitude:Float = 0f
        if(viewmodel.getSavedMarker()==null){
            findNavController().navigate(R.id.mapFragment)
        }
        longitude = viewmodel.getSavedMarker()!!.lon!!
        latitude = viewmodel.getSavedMarker()!!.lat!!
        binding.coordenades.text = "[${String.format("%.4f", latitude)}, ${String.format("%.4f", longitude)}]"
        binding.titleName.setText(viewmodel.getSavedMarker()!!.name)

        val listColors = resources.getStringArray(R.array.color_spinner)
        val adapterSpinner = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, listColors)
        binding.colorSelect.adapter = adapterSpinner
        binding.colorSelect.setSelection(adapterSpinner.getPosition(viewmodel.textFromValue(viewmodel.getSavedMarker()!!.color!!)))
        binding.colorSelect.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when(listColors[p2]){
                    "Yellow"-> markerColor = BitmapDescriptorFactory.HUE_YELLOW
                    "Red"-> markerColor = BitmapDescriptorFactory.HUE_RED
                    "Orange"-> markerColor = BitmapDescriptorFactory.HUE_ORANGE
                    "Purple"-> markerColor = BitmapDescriptorFactory.HUE_MAGENTA
                }
                //println(markerColor)
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
                markerColor = viewmodel.getSavedMarker()!!.color!!
                //println(markerColor)
            }
        }
        if(viewmodel.getSavedMarker()!!.image != null){
            binding.AddImage.visibility = View.GONE
            //binding.cameraImage.setImageURI(viewmodel.getSavedMarker()!!.image)
            binding.cameraImage.setImageBitmap(BitmapFactory.decodeFile(Uri.parse(viewmodel.getSavedMarker()!!.image!!).path))
            binding.cameraImage.rotation = 90f
        }
        binding.AddImage.setOnClickListener{
            viewmodel.saveImageData(binding.titleName.text.toString(), latitude, longitude, markerColor)
            findNavController().navigate(R.id.cameraFragment)
        }
        binding.acceptButton.setOnClickListener{
            if(viewmodel.getSavedMarker()!!.image!=null){
                val image = viewmodel.getSavedMarker()!!.image
                val marker = Marker(lat = latitude!!, lon = longitude!!, color =  markerColor, name = binding.titleName.text.toString(), image = "")
                viewmodel.addToList(marker)
                //returnToMap(latitude, longitude, markerColor, true)
                db.collection("markers").add(marker).addOnSuccessListener{
                    println("Marcador crat amb ID: ${it.id}")
                    println("Imatge: "+marker.image)
                    val storage = FirebaseStorage.getInstance().getReference("images/${it.id}")

                    storage.putFile(Uri.parse(image))
                        .addOnSuccessListener {
                            Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                        }
                        .addOnFailureListener {
                            Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                        }

                }.addOnFailureListener { e ->
                    Log.w("ERROR", "Error creant el marcador", e)
                }

                findNavController().navigate(R.id.mapFragment)
            }else{
                Toast.makeText(requireContext(),"No image added.", Toast.LENGTH_LONG).show()
            }

        }
        binding.cancelButton.setOnClickListener{
            //returnToMap(null, null, null, false)
            findNavController().navigate(R.id.mapFragment)
        }
    }
}