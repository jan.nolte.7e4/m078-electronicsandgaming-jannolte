package cat.itb.electronicsandgaming.Fragments.Map;

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.electronicsandgaming.Data.Marker
import cat.itb.electronicsandgaming.R
import cat.itb.electronicsandgaming.SingletonMarkers
import cat.itb.electronicsandgaming.databinding.MarkerEditBinding
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class MarkerEdit  : Fragment() {
    private val viewmodel: SingletonMarkers by activityViewModels()
    private lateinit var binding: MarkerEditBinding
    private val db = FirebaseFirestore.getInstance()
    private lateinit var editableMarker : Marker
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = MarkerEditBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val marker_id = arguments?.getString("id_marker")

        db.collection("markers").document(marker_id!!).get().addOnSuccessListener {
            //println((it.get("lon") as Double).toFloat())
            editableMarker = Marker(lon=(it.get("lon") as Double).toFloat(), lat = (it.get("lat") as Double).toFloat(), name = it.get("name") as String, color = (it.get("color") as Double).toFloat(), image = "")
            binding.titleName.setText(editableMarker.name!!)
            binding.coordenades.text = "[${String.format("%.4f", editableMarker.lat)}, ${String.format("%.4f", editableMarker.lon)}]"
            val listColors = resources.getStringArray(R.array.color_spinner)
            val adapterSpinner = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, listColors)
            binding.colorSelect.adapter = adapterSpinner
            binding.colorSelect.setSelection(adapterSpinner.getPosition(viewmodel.textFromValue(editableMarker.color!!)))
            binding.colorSelect.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    when(listColors[p2]){
                        "Yellow"-> editableMarker.color = BitmapDescriptorFactory.HUE_YELLOW
                        "Red"-> editableMarker.color = BitmapDescriptorFactory.HUE_RED
                        "Orange"-> editableMarker.color = BitmapDescriptorFactory.HUE_ORANGE
                        "Purple"-> editableMarker.color = BitmapDescriptorFactory.HUE_MAGENTA
                    }
                    //println(markerColor)
                }
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    println(editableMarker.color)
                }
            }
            val storage = FirebaseStorage.getInstance().reference.child("images/${marker_id}")
            val localFile = File.createTempFile("temp", "jpeg")
            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                binding.cameraImage.rotation=90f
                binding.cameraImage.setImageBitmap(bitmap)
            }.addOnFailureListener{
                Toast.makeText(context, "Error downloading image!", Toast.LENGTH_SHORT)
                    .show()
            }
            binding.acceptButton.setOnClickListener{
                editableMarker.name = binding.titleName.text.toString()
                db.collection("markers").document(marker_id).set(editableMarker).addOnSuccessListener {
                    println("Marker updated")
                    navigateBacktoList()
                }.addOnFailureListener { e ->
                    Log.w("ERROR", "Error creant el marcador", e)
                }
            }
            binding.cancelButton.setOnClickListener{
                navigateBacktoList()
            }
            binding.deleteMarkerButton.setOnClickListener{
                println("Button pressed")
                val alertDialog = AlertDialog.Builder(context)
                alertDialog
                    .setTitle("Confirmation")
                    .setMessage("Are you sure you want to delete this marker?")
                    .setPositiveButton("Yes", DialogInterface.OnClickListener{ dialog, id ->
                        if(deleteThisMarker(marker_id)){
                            Toast.makeText(context, "Marker deleted!", Toast.LENGTH_SHORT)
                                .show()
                            navigateBacktoList()
                        }else{
                            Toast.makeText(context, "Marker still alive!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    })
                    .setNegativeButton("No", DialogInterface.OnClickListener{ dialog, id ->
                        // Nothing deleted
                    })
                alertDialog.create().show()
            }
        }
    }
    fun deleteThisMarker(idMarker:String):Boolean{
        var noProblem = true
        CoroutineScope(Dispatchers.IO).launch {
            db.collection("markers").document(idMarker).delete()
            val storage = FirebaseStorage.getInstance().reference.child("images/${idMarker}")
            storage.delete().addOnSuccessListener {
                noProblem = true
            }.addOnFailureListener{
                noProblem = false
            }
        }
        return noProblem
    }
    fun navigateBacktoList(){
        val directions = MarkerEditDirections.actionMarkerEditToMarkerList()
        findNavController().navigate(directions)
    }
}
