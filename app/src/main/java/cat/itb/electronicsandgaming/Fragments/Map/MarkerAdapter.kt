package cat.itb.electronicsandgaming.Fragments.Map

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import cat.itb.electronicsandgaming.Data.Marker
import cat.itb.electronicsandgaming.R
import cat.itb.electronicsandgaming.SingletonMarkers
import cat.itb.electronicsandgaming.databinding.OneitemFromlistBinding
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.*
import java.io.File

class MarkerAdapter(private var markers:List<Marker>): RecyclerView.Adapter<MarkerAdapter.MarkerViewHolder>() {
    lateinit var binding : OneitemFromlistBinding
    lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarkerViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.oneitem_fromlist, parent, false)
        context = parent.context
        binding = OneitemFromlistBinding.bind(view)
        return MarkerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return markers.size
    }

    override fun onBindViewHolder(holder: MarkerViewHolder, position: Int) {
        val marker = markers[position]
        holder.setIsRecyclable(false)
        holder.bind(marker)
        holder.itemView.setOnClickListener{
            val directions = Recycler_FragmentDirections.actionMarkerListToMarkerEdit(
                    marker.id!!
                )
            it.findNavController().navigate(directions)
            //view -> view.findNavController().navigate(R.id.action_markerList_to_markerEdit)
        }
    }
    fun clearMarkerList(){
        markers = listOf<Marker>()
        notifyDataSetChanged()
    }
    fun setMarkerList(m:List<Marker>){
        markers = m
        notifyDataSetChanged()
    }

    inner class MarkerViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(marker: Marker) {
            binding.markerTitle.text = marker.name
            binding.CoordinadesText.text = "Latitude: ${marker.lat} Longitude: ${marker.lon}"
            binding.cameraImage.setImageResource(selectImage(marker.color))
            /*val storage = FirebaseStorage.getInstance().reference.child("images/${marker.id}")
            val localFile = File.createTempFile("temp", "jpeg")
            var bitmap: Bitmap? = null
            CoroutineScope(Dispatchers.IO).launch{
                storage.getFile(localFile).addOnSuccessListener {
                    bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                    println(bitmap)
                    binding.cameraImage.setImageBitmap(bitmap)
                    binding.cameraImage.rotation = 90f
                    //Glide.with(context).load(bitmap).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().into(binding.cameraImage)
                }.addOnFailureListener {
                    Toast.makeText(
                        context,
                        "Error downloading image ${marker.id}!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }

            //Glide.with(context).load(bitmap).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().into(binding.cameraImage)
            if(marker.image!=null){
                    binding.cameraImage.setImageBitmap(BitmapFactory.decodeFile(Uri.parse(marker.image!!).path))
                    binding.cameraImage.rotation=90f
                }*/

        }

        private fun selectImage(color: Float?): Int {
            when(color){
                BitmapDescriptorFactory.HUE_RED -> return R.drawable.rupia_roja
                BitmapDescriptorFactory.HUE_YELLOW -> return R.drawable.rupia_amarilla
                BitmapDescriptorFactory.HUE_ORANGE -> return R.drawable.rupia_naranja
                BitmapDescriptorFactory.HUE_MAGENTA -> return R.drawable.rupia_lila
                else -> return R.drawable.rupia_rara
            }
        }
    }

}