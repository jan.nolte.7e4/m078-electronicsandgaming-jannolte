package cat.itb.electronicsandgaming.Fragments.Login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cat.itb.electronicsandgaming.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth

class RegisterFragment: Fragment() {
    private lateinit var binding: FragmentRegisterBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val email = arguments?.getString("email")
        /*if(email!=null){
            binding.editTextTextEmailAddress.setText(email)
        }*/
        binding.haveAnAccountText.setOnClickListener{
            val email = binding.editTextTextEmailAddress.text.toString()
            gotoLogIn(email)
        }
        binding.continueButton.setOnClickListener {
            val email = binding.editTextTextEmailAddress.text.toString()
            val password = binding.editTextTextPassword.text.toString()
            FirebaseAuth
                .getInstance()
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        gotoLogIn(emailLogged!!)
                    }
                    else{
                        println("Error al registrar l'usuari")
                    }
                }

        }
    }

    private fun gotoLogIn(emailLogged: String) {
        val directions = RegisterFragmentDirections.actionRegisterFragmentToLogInFragment(emailLogged)
        findNavController().navigate(directions)
    }

}