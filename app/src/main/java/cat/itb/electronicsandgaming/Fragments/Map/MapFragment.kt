package cat.itb.electronicsandgaming.Fragments.Map

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import cat.itb.electronicsandgaming.databinding.FragmentMapBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.electronicsandgaming.Data.Marker
import cat.itb.electronicsandgaming.SingletonMarkers
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


const val REQUEST_CODE_LOCATION = 100
class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var binding: FragmentMapBinding
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var startingPosition: LatLng
    private val viewmodel: SingletonMarkers by activityViewModels()
    private val db = FirebaseFirestore.getInstance()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapBinding.inflate(layoutInflater)
        createMap()
        viewmodel.resetSave()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        return binding.root
    }

    fun createMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(binding.map.id) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setOnMapLongClickListener {
            println(it.latitude.toString() + ", " + it.longitude.toString())
            val directions = MapFragmentDirections.actionMapFragmentToMarkerCreation()
            viewmodel.saveMarker(
                Marker(
                    id = null,
                    it.latitude.toFloat(),
                    it.longitude.toFloat(),
                    image = null
                )
            )
            findNavController().navigate(directions)
        }
        //createMarkers(viewmodel.listOfMarkers())
        createMarkers()
        enableLocation()
        getDeviceLocation()
        println("location enabled")
    }

    private fun focusLocation(location: LatLng) {
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(location, 18f),
            5000,
            null
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*binding.button2.setOnClickListener{
            val directions = MapFragmentDirections.actionMapFragmentToRecyclerFragment()
            findNavController().navigate(directions)
        }*/
    }

    fun createMarkers() {
        /*viewmodel.updateList()
        viewmodel.markersLiveData.observe(viewLifecycleOwner, Observer{
            for(marker in it){
                println(" -> ${marker.id}, ${marker.lat}, ${marker.lon}")
                createMarker(LatLng(marker.lat!!.toDouble(), marker.lon!!.toDouble()),marker.name!!,marker.color!!)
            }
        })*/
        CoroutineScope(Dispatchers.IO).launch {
            db.collection("markers").addSnapshotListener(object : EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                    if (error != null) {
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    db.collection("markers").get().addOnSuccessListener {
                        for (document in it) {
                            val newMarker = document.toObject(Marker::class.java)
                            newMarker.id = document.id
                            println(document.id)
                            createMarker(
                                LatLng(
                                    newMarker.lat!!.toDouble(),
                                    newMarker.lon!!.toDouble()
                                ), newMarker.name!!, newMarker.color!!
                            )
                        }
                    }
                }
            })
        }
    }

    // Function to create Markers
    fun createMarker(coordinates: LatLng, title: String, markerColor: Float) {
        val myMarker = MarkerOptions().position(coordinates).title(title)
            .icon(BitmapDescriptorFactory.defaultMarker(markerColor))
        map.addMarker(myMarker)
        //map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 18f),5000, null)
    }

    // Load, setup and handle permissions from the map
    @SuppressLint("MissingPermission")
    private fun enableLocation() {
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            map.isMyLocationEnabled = true
        } else {
            requestLocationPermission()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                map.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                requireContext(),
                "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }
    }

    @SuppressLint("MissingPermission")
    fun getDeviceLocation() {
        val locationResult = fusedLocationProviderClient.lastLocation
        this.activity?.let {
            locationResult.addOnCompleteListener(it) { task ->
                if (task.isSuccessful) {
                    // Set the map's camera position to the current location of the device.
                    startingPosition = LatLng(task.result.latitude, task.result.longitude)
                    focusLocation(startingPosition)
                }
            }
        }
    }
}