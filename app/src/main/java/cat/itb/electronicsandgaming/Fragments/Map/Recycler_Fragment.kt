package cat.itb.electronicsandgaming.Fragments.Map

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cat.itb.electronicsandgaming.Data.Marker
import cat.itb.electronicsandgaming.R
import cat.itb.electronicsandgaming.SingletonMarkers
import cat.itb.electronicsandgaming.databinding.MarkerListBinding
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.*

class Recycler_Fragment: Fragment() {
    private lateinit var binding: MarkerListBinding
    private lateinit var recyclerView: RecyclerView
    private val viewmodel: SingletonMarkers by activityViewModels()
    private val db = FirebaseFirestore.getInstance()
    private lateinit var markerList :ArrayList<Marker>
    private lateinit var markerAdapter: MarkerAdapter
    private var selectedColor: Float? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = MarkerListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView(arrayListOf())

        val listColors = resources.getStringArray(R.array.color_spinner_recycler)
        val adapterSpinner = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, listColors)
        binding.colorselector.adapter = adapterSpinner
        binding.colorselector.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when(listColors[p2]){
                    "Yellow"-> {
                        selectedColor= BitmapDescriptorFactory.HUE_YELLOW
                    }
                    "Red"-> {
                        selectedColor = BitmapDescriptorFactory.HUE_RED
                    }
                    "Orange"-> {
                        selectedColor = BitmapDescriptorFactory.HUE_ORANGE
                    }
                    "Purple"-> {
                        selectedColor = BitmapDescriptorFactory.HUE_MAGENTA
                    }
                    "All"->{
                        selectedColor = null
                    }
                }
                eventChangeListener(selectedColor)
                //println(markerColor)
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
                selectedColor = null
                eventChangeListener(selectedColor)
                //println(markerColor)
            }
        }

    }

    private fun eventChangeListener(color:Float?) {
        //markerAdapter.clearMarkerList()
        markerList = arrayListOf<Marker>()
        if(color == null){
            db.collection("markers").addSnapshotListener(object: EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                    if(error != null){
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    for(dc: DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            val newMarker = dc.document.toObject(Marker::class.java)
                            newMarker.id = dc.document.id
                            markerList.add(newMarker)
                        }
                    }
                    markerAdapter.setMarkerList(markerList.toList())
                }
            })
        }else{
            println(color.toInt())
            db.collection("markers").whereEqualTo("color", color).addSnapshotListener(object: EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                    if(error != null){
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    for(dc: DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            val newMarker = dc.document.toObject(Marker::class.java)
                            newMarker.id = dc.document.id
                            markerList.add(newMarker)
                        }
                    }
                    markerAdapter.setMarkerList(markerList.toList())
                }
            })
        }
    }

    fun setupRecyclerView(markerList:List<Marker>) {
        markerAdapter = MarkerAdapter(markerList)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = markerAdapter
        }
    }
}