package cat.itb.electronicsandgaming.Fragments.Login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cat.itb.electronicsandgaming.MapActivity
import cat.itb.electronicsandgaming.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth

class LogInFragment:Fragment() {
    private lateinit var binding: FragmentLoginBinding
    lateinit var myPreferences: SharedPreferences
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //myPreferences = requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        myPreferences = activity?.getPreferences(Context.MODE_PRIVATE)!!
        var email = myPreferences.getString("email", "")
        var emailRegister = arguments?.getString("email")
        var passText = myPreferences.getString("password", "")
        var rememberState = myPreferences.getBoolean("remember", false)
        //val email = arguments?.getString("email")
        if(emailRegister!=null){
            binding.editTextTextEmailAddress.setText(emailRegister)
        }else{
            if(email!=null){
                if(email.isNotEmpty()){
                    binding.editTextTextEmailAddress.setText(email)
                    binding.editTextTextPassword.setText(passText)
                    binding.switch1.isChecked = rememberState
                }
            }
        }

        binding.textView3.setOnClickListener{
            gotoRegister(binding.editTextTextEmailAddress.text.toString())
        }
        binding.continueButton.setOnClickListener{
            email = binding.editTextTextEmailAddress.text.toString()
            passText = binding.editTextTextPassword.text.toString()
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email!!, passText!!)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        if(binding.switch1.isChecked){
                            myPreferences.edit {
                                putString("email", email)
                                putString("password", passText)
                                putBoolean("remember", binding.switch1.isChecked)
                                apply()
                            }
                            println("Saved preferences")
                        }else{
                            myPreferences.edit {
                                putString("email", "")
                                putString("password", "")
                                putBoolean("remember", false)
                                apply()
                            }
                        }
                        val intent = Intent(requireContext(), MapActivity::class.java)
                        startActivity(intent)
                    }
                    else{
                        println("Error al fer login")
                        binding.editTextTextPassword.setText("")
                        Toast.makeText(context, "User or Password not correct", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
    private fun gotoRegister(emailLogged: String) {
        val directions = LogInFragmentDirections.actionLogInFragmentToRegisterFragment(emailLogged)
        findNavController().navigate(directions)
    }
}