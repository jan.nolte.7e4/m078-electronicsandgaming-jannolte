package cat.itb.electronicsandgaming.Data

import android.net.Uri
import com.google.android.gms.maps.model.BitmapDescriptorFactory

data class Marker (
    var id: String? = null,
    var lat:Float? = null,
    var lon:Float? = null,
    var color:Float? = BitmapDescriptorFactory.HUE_RED,
    var name:String? = "Title",
    var image: String? = null
    )